# The Travel Calculator App
[![pipeline status](https://gitlab.com/madmax_inc/travelcalculator/badges/master/pipeline.svg)](https://gitlab.com/madmax_inc/travelcalculator/commits/master)
[![coverage report](https://gitlab.com/madmax_inc/travelcalculator/badges/master/coverage.svg)](https://gitlab.com/madmax_inc/travelcalculator/commits/master)

# Setting up a development environment

Just use the following command and open `http://localhost:8000` in your web browser =)
```bash
docker-compose -f docker-compose.dev.yml up --build
```
