#!/bin/bash
docker build -t travelcalculator-frontend:dev Docker -f Docker/Dockerfile.dev && docker run -v $(pwd):/app -p 3000:3000 travelcalculator-frontend:dev